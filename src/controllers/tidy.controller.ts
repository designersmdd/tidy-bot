import tidyService from '@services/tidy.service';
import { NextFunction, Request, Response } from 'express';

class TidyController {
  public tidyService = new tidyService();

  public getTidyStudentsOfThisWeek = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const data = this.tidyService.getTidyStudents();
      res.status(200).json(data);
    } catch (error) {
      next(error);
    }
  };

  public getTidySchedule = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const data = this.tidyService.getTidySchedule();
      res.status(200).json({ data });
    } catch (error) {
      next(error);
    }
  };
}

export default TidyController;
