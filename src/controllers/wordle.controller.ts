import { WordleService } from '@/services/wordle.service';
import { NextFunction, Request, Response } from 'express';

export class WordleController {
  public wordleService = new WordleService();

  public getWords = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const words = await this.wordleService.findAllWords();
      res.status(200).json({ data: words });
    } catch (error) {
      next(error);
    }
  };
}
