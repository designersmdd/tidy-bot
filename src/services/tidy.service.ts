import { HttpException } from '@/exceptions/HttpException';
import { Student } from '@/interfaces/student.interface';
import fs from 'fs';

const studentFile = './public/students.json';

const mapStudent = (student: Student): Student => {
  const begin = student.begindate.toString().split('/'); // Is in DD/MM/YYYY format
  const end = student.enddate.toString().split('/'); // Is in DD/MM/YYYY format

  const begindate = new Date(`${begin[1]}-${begin[0]}-${begin[2]}`);
  const enddate = new Date(`${end[1]}-${end[0]}-${end[2]}`);

  return {
    ...student,
    begindate,
    enddate,
  };
};

const currentWeekFiler = (student: Student): boolean => {
  const now = new Date();
  now.setDate(now.getDate());

  if (now < new Date(student.begindate)) return false;
  if (now > new Date(student.enddate)) return false;

  return true;
};

const previousWeekFilter = (student: Student): boolean => {
  const now = new Date();
  now.setDate(now.getDate() - 7);

  if (now < new Date(student.begindate)) return false;
  if (now > new Date(student.enddate)) return false;

  return true;
};

class TidyService {
  private students: Student[] = [];

  public getTidyStudents(): { currentWeek: Student[]; previousWeek: Student[] } {
    try {
      const data = fs.readFileSync(studentFile, 'utf-8');
      this.students = JSON.parse(data).caretakers;
      this.students = this.students.map(mapStudent);

      const currentWeek = this.students.filter(currentWeekFiler);
      const previousWeek = this.students.filter(previousWeekFilter);

      return { currentWeek, previousWeek };
    } catch {
      throw new HttpException(500, `Can't read ${studentFile}`);
    }
  }

  public getTidySchedule(): Student[] {
    try {
      const data = fs.readFileSync(studentFile, 'utf-8');
      this.students = JSON.parse(data).caretakers;
      this.students = this.students.map(mapStudent);

      return this.students;
    } catch {
      throw new HttpException(500, `Can't read ${studentFile}`);
    }
  }
}

export default TidyService;
