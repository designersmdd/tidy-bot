import fs from 'fs';

export class WordleService {
  public findAllWords = async (lang = 'en'): Promise<string[]> => {
    const file = `./public/words.${lang}.json`;
    const data = fs.readFileSync(file, 'utf-8');

    return JSON.parse(data).words;
  };
}
