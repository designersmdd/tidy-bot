import { WordleController } from '@/controllers/wordle.controller';
import { Routes } from '@/interfaces/routes.interface';
import { Router } from 'express';

export class WordleRoute implements Routes {
  public path = '/wordle';
  public router = Router();
  public wordleController = new WordleController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}`, this.wordleController.getWords);
  }
}
