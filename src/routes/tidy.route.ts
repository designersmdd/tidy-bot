import TidyController from '@/controllers/tidy.controller';
import { Routes } from '@/interfaces/routes.interface';
import { Router } from 'express';

export class TidyRoute implements Routes {
  public path = '/tidy';
  public router = Router();
  public tidyController = new TidyController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}/now`, this.tidyController.getTidyStudentsOfThisWeek);
    this.router.get(`${this.path}/schedule`, this.tidyController.getTidySchedule);
  }
}
