import App from '@/app';
import AuthRoute from '@routes/auth.route';
import IndexRoute from '@routes/index.route';
import UsersRoute from '@routes/users.route';
import validateEnv from '@utils/validateEnv';
import { TidyRoute } from '@routes/tidy.route';
import { WordleRoute } from '@routes/wordle.route';

validateEnv();

const app = new App([new IndexRoute(), new UsersRoute(), new AuthRoute(), new TidyRoute(), new WordleRoute()]);

app.listen();
