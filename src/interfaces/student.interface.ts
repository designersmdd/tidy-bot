export interface Student {
  name: string;
  email: string;
  begindate: Date;
  enddate: Date;
}
